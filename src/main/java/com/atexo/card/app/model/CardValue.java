package com.atexo.card.app.model;

/**
 * 
 * 
 * @author SGHA99K
 *
 */
public class CardValue extends CardAttribute {

	public static final String VALUE = "VALUE";

	public CardValue(String value) {
		super(value);
	}

	public CardValue(Integer ord, String value) {
		super(ord, value);
	}

	public String getName() {
		return VALUE;
	}

}
