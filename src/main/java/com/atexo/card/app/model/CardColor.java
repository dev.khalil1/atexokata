package com.atexo.card.app.model;

/**
 * 
 * @author SGHA99K
 *
 */
public class CardColor extends CardAttribute {
	public static final String COLOR = "COLOR";

	public CardColor(String color) {
		super(color);
	}

	public CardColor(Integer ord, String color) {
		super(ord, color);
	}

	@Override
	public String getName() {
		return COLOR;
	}

}
