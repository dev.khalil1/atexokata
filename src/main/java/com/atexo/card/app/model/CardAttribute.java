package com.atexo.card.app.model;

/**
 *  
 * Behavior class for sorting management 
 * 
 * @author SGHA99K
 *
 */
public abstract class CardAttribute {
	
	private Integer ord;
	private String value;
	
	abstract String getName();
	
	/**
	 * default constructor
	 */
	public CardAttribute() {
		
	}
	
	/**
	 * constructor
	 * 
	 * @param value
	 */
	public CardAttribute(String value) {
		super();
		this.value = value;
	}
	
	/**
	 * constructor
	 * 
	 * @param ord
	 * @param value
	 */
	public CardAttribute(Integer ord, String value) {
		super();
		this.ord = ord;
		this.value = value;
	}
	
	/**
	 * @return the ord
	 */
	public Integer getOrd() {
		return ord;
	}
	/**
	 * @param ord the ord to set
	 */
	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString() {
		return value;
	}

}
