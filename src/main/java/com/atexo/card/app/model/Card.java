/**
 * 
 */
package com.atexo.card.app.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Class representing the business unit to be handled
 * 
 * @author SGHA99K
 *
 */
public class Card {

	private Map<String, CardAttribute> cardAttributes = new HashMap<>();

	public Card() {

	}

	/**
	 * Constructor
	 * 
	 * @param cardAtts
	 */
	public Card(CardAttribute... cardAtts) {
		super();
		for (CardAttribute cardAttribute : cardAtts) {
			cardAttributes.put(cardAttribute.getName(), cardAttribute);
		}
	}

	/**
	 * Get Attributes
	 * 
	 * @return map of attribute by name of attribute
	 */
	public Map<String, CardAttribute> getCardAttributes() {
		return cardAttributes;
	}

	public void setCardAttributes(Map<String, CardAttribute> cardAttributes) {
		this.cardAttributes = cardAttributes;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		for (Iterator<Entry<String, CardAttribute>> iterator = cardAttributes.entrySet().iterator(); iterator
				.hasNext();) {
			Entry<String, CardAttribute> entry = iterator.next();
			sb.append(entry.getValue().toString());
			if (iterator.hasNext()) {
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString();
	}

}
