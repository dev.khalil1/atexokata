package com.atexo.card.app.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Enum of card values
 * 
 * @author SGHA99K
 *
 */
public enum CARDVALUES {
	TWO("TWO"), THREE("THREE"), FOUR("FOUR"), FIVE("FIVE"), SIX("SIX"), SEVEN("SEVEN"), EIGHT("EIGHT"), NINE(
			"NINE"), TEN("TEN"), JACK("JACK"), QUEEN("QUEEN"), KING("KING"), ACE("ACE");

	String name;

	CARDVALUES(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static CARDVALUES getCardValuesName(String name) {
		for (CARDVALUES cardValue : CARDVALUES.values()) {
			if (cardValue.getName().equals(name)) {
				return cardValue;
			}
		}
		return null;
	}

	public static List<String> getCardValuesNames() {
		List<String> enumNames = Stream.of(CARDVALUES.values()).map(CARDVALUES::name).collect(Collectors.toList());
		return enumNames;
	}
}
