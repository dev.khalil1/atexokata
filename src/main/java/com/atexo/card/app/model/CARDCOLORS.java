package com.atexo.card.app.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Enum of card colors
 * 
 * @author SGHA99K
 *
 */
public enum CARDCOLORS {
	CLUB("CLUB"), DIAMOND("DIAMOND"), HEARTS("HEARTS"), SPADE("SPADE");
	String name;

	CARDCOLORS(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static CARDCOLORS getCardColorsName(String name) {
		for (CARDCOLORS cardColor : CARDCOLORS.values()) {
			if (cardColor.getName().equals(name)) {
				return cardColor;
			}
		}
		return null;
	}

	public static List<String> getCardColorsNames() {
		List<String> enumNames = Stream.of(CARDCOLORS.values()).map(CARDCOLORS::name).collect(Collectors.toList());
		return enumNames;
	}

}
