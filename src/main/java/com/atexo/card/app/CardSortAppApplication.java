package com.atexo.card.app;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.atexo.card.app.model.Card;
import com.atexo.card.app.model.CardColor;
import com.atexo.card.app.model.CardValue;
import com.atexo.card.app.services.Game;

@SpringBootApplication
public class CardSortAppApplication {

	@Autowired
	private Game game;

	public static void main(String[] args) {
		SpringApplication.run(CardSortAppApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {

			game.shuffleCardColor(); // build a random order of colors
			game.shuffleCardValue(); // build a random order of values
			System.out.println("ordre de couleur aleatoire:\n" + game.getCardColorOrder());
			System.out.println("ordre de valeur aleatoire:\n" + game.getCardValueOrder());
			// build hand of cards
			List<Card> buildTenCardHand = new ArrayList<>(game.buildTenCardHand(game.getPackOfCards()));
			System.out.println("main aleatoire:\n" + buildTenCardHand);
			game.sortCardsByColor(buildTenCardHand); //  sort by color
			System.out.println("carte triees par couleur:\n" + buildTenCardHand);
			game.sortCardsByValue(buildTenCardHand); // sort by value
			System.out.println("carte triees par valeur:\n" + buildTenCardHand);
			// sort by attributes (color, value)
			game.sortCardsByAttributes(buildTenCardHand, CardColor.COLOR, CardValue.VALUE);
			System.out.println("carte triees par couleur et valeur:\n" + buildTenCardHand);
			System.exit(0);
			
		};
	}
}
