package com.atexo.card.app.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.atexo.card.app.model.Card;
import com.atexo.card.app.model.CardColor;
import com.atexo.card.app.model.CARDCOLORS;
import com.atexo.card.app.model.CardValue;
import com.atexo.card.app.model.CARDVALUES;

@Service
public class Game {

	public static int HAND_SIZE = 10;
	
	// list of card values
	private List<String> cardValueOrder = new ArrayList<>(CARDVALUES.getCardValuesNames());
	// list of card colors
	private List<String> cardColorOrder = new ArrayList<>(CARDCOLORS.getCardColorsNames());

	/**
	 * default constructor 
	 */
	public Game() {
		super();
	}
	
	/**
	 * Initialize the deck of cards 
	 */
	public List<Card> getPackOfCards() {
		List<Card> crdLst = new ArrayList<>();
		for (String color : CARDCOLORS.getCardColorsNames()) {
			for (String value : CARDVALUES.getCardValuesNames()) {
				Card cd = new Card(new CardValue(getCardValueOrder().indexOf(value), value),
						new CardColor(getCardColorOrder().indexOf(color), color));
				crdLst.add(cd);
			}
		}
		return crdLst;
	}
	
	/**
	 * Build a random order of colors
	 */
	public void shuffleCardColor() {
		Collections.shuffle(cardColorOrder);
	}

	/**
	 * Build a random order of values
	 */
	public void shuffleCardValue() {
		Collections.shuffle(cardValueOrder);
	}

	/**
	 * Build a 10 random cards from the list of cards
	 * 
	 * @param crdLst list of cards
	 * @return list of 10 cards 
	 */
	public List<Card> buildTenCardHand(List<Card> crdLst) {
		List<Card> tenCardHand = new ArrayList<>();
		Random rand = new Random();
		for (int i = 0; i < HAND_SIZE; i++) {
			int randomIndex = rand.nextInt(crdLst.size());
			Card randomCard = crdLst.get(randomIndex);
			tenCardHand.add(randomCard);
			crdLst.remove(randomIndex);
		}
		return tenCardHand;
	}

	/**
	 * Sort the list of cards in order of colors 
	 * 
	 * @param crdLst list of cards
	 */
	public void sortCardsByColor(List<Card> lstCards) {
		sortCardsByAttributes(lstCards, CardColor.COLOR);
	}

	/**
	 * Sort the list of cards in order of values 
	 * 
	 * @param crdLst list of cards
	 */
	public void sortCardsByValue(List<Card> lstCards) {
		sortCardsByAttributes(lstCards, CardValue.VALUE);
	}
	
	/**
	 * Sort the list of cards in order list (values, colors) 
	 * 
	 * @param crdLst list of cards
	 * @param attributes list of order 
	 */
	public void sortCardsByAttributes(List<Card> lstCards, String... attributes) {
		lstCards.sort((o1, o2) -> {
			int cmp = 0;
			int index = 0;
			while (index < attributes.length) {
				cmp = o1.getCardAttributes().get(attributes[index]).getOrd()
						.compareTo(o2.getCardAttributes().get(attributes[index]).getOrd());
				if (cmp != 0) {
					break;
				}
				index++;
			}
			return cmp;
		});
	}

	/**
	 * Get the size of hand
	 */
	public int getHandSize() {
		return HAND_SIZE;
	}
	
	/**
	 * Get the list of order values 
	 */
	public List<String> getCardValueOrder() {
		return cardValueOrder;
	}
	
	/**
	 * Get the list of order colors
	 */
	public List<String> getCardColorOrder() {
		return cardColorOrder;
	}
}
