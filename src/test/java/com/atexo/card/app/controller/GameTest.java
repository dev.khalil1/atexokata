package com.atexo.card.app.controller;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.atexo.card.app.model.Card;
import com.atexo.card.app.model.CardColor;
import com.atexo.card.app.model.CardValue;
import com.atexo.card.app.services.Game;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

	@InjectMocks
	Game game;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	//
	// @Test
	// public void color_order_may_change() {
	// // System.out.println(Arrays.asList(game.CARD_COLORS));
	// game.shuffleCardColor();
	// // System.out.println(game.getCardColorOrder());
	// }
	//
	// @Test
	// public void value_order_may_change() {
	// // System.out.println(Arrays.asList(game.CARD_VALUES));
	// game.shuffleCardValue();
	// // System.out.println(game.getCardValueOrder());
	// }

	@Test
	public void hand_should_contain_10_cards() {
		List<Card> cardsPack = game.getPackOfCards();
		List<Card> cards = game.buildTenCardHand(cardsPack);
		assertEquals(game.getHandSize(), cards.size());
	}

	@Test
	public void cards_should_be_sorted_by_color() {
		// String expectedResult = "[Card [{COLOR=[ord=0, color=CLUB], VALUE=[ord=1,
		// value=ACE]}], Card [{COLOR=[ord=2, color=DIAMOND], VALUE=[ord=6,
		// value=SIX]}], Card [{COLOR=[ord=3, color=HEARTS], VALUE=[ord=11,
		// value=JACK]}], Card [{COLOR=[ord=3, color=HEARTS], VALUE=[ord=2,
		// value=TEN]}]]";
		String expectedResult = "[[CLUB,ACE], [DIAMOND,SIX], [HEARTS,JACK], [HEARTS,TEN]]";
		List<Card> cardList = Arrays
				.asList(new Card[] { new Card(new CardColor(3, "HEARTS"), new CardValue(11, "JACK")),
						new Card(new CardColor(2, "DIAMOND"), new CardValue(6, "SIX")),
						new Card(new CardColor(3, "HEARTS"), new CardValue(2, "TEN")),
						new Card(new CardColor(0, "CLUB"), new CardValue(1, "ACE")) });

		game.sortCardsByColor(cardList);
		assertEquals(expectedResult, cardList.toString());
	}

	@Test
	public void cards_should_be_sorted_by_value() {
		// String expectedResult = "[Card [{COLOR=[ord=0, color=CLUB], VALUE=[ord=1,
		// value=ACE]}], Card [{COLOR=[ord=3, color=HEARTS], VALUE=[ord=2, value=TEN]}],
		// Card [{COLOR=[ord=2, color=DIAMOND], VALUE=[ord=6, value=SIX]}], Card
		// [{COLOR=[ord=3, color=HEARTS], VALUE=[ord=11, value=JACK]}]]";
		String expectedResult = "[[CLUB,ACE], [HEARTS,TEN], [DIAMOND,SIX], [HEARTS,JACK]]";
		List<Card> cardList = Arrays
				.asList(new Card[] { new Card(new CardColor(3, "HEARTS"), new CardValue(11, "JACK")),
						new Card(new CardColor(2, "DIAMOND"), new CardValue(6, "SIX")),
						new Card(new CardColor(3, "HEARTS"), new CardValue(2, "TEN")),
						new Card(new CardColor(0, "CLUB"), new CardValue(1, "ACE")) });

		game.sortCardsByValue(cardList);
		assertEquals(expectedResult, cardList.toString());
	}

	@Test
	public void cards_should_be_sorted_by_color_and_value() {
		// String expectedResult = "[Card [{COLOR=[ord=0, color=CLUB], VALUE=[ord=1,
		// value=ACE]}], Card [{COLOR=[ord=2, color=DIAMOND], VALUE=[ord=6,
		// value=SIX]}], Card [{COLOR=[ord=3, color=HEARTS], VALUE=[ord=2, value=TEN]}],
		// Card [{COLOR=[ord=3, color=HEARTS], VALUE=[ord=11, value=JACK]}]]";
		String expectedResult = "[[CLUB,ACE], [DIAMOND,SIX], [HEARTS,TEN], [HEARTS,JACK]]";
		List<Card> cardList = Arrays
				.asList(new Card[] { new Card(new CardColor(3, "HEARTS"), new CardValue(11, "JACK")),
						new Card(new CardColor(2, "DIAMOND"), new CardValue(6, "SIX")),
						new Card(new CardColor(3, "HEARTS"), new CardValue(2, "TEN")),
						new Card(new CardColor(0, "CLUB"), new CardValue(1, "ACE")) });

		game.sortCardsByAttributes(cardList, "COLOR", "VALUE");
		assertEquals(expectedResult, cardList.toString());
	}

}
